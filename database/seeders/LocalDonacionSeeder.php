<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocalDonacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('local_donacion')->insert([
            'local_donacion' => "EL CANTARO",
            'longitud' => -56.820182,
            'latitud' => -27.387026,
            'direccion' => "Av. Paraguay Casi Mcal. Lopez 343",
            'hora_apertura' => 8,
            'hora_cierre' => 21,
        ]);
        DB::table('local_donacion')->insert([
            'local_donacion' => "FCA - CAMPUS UNP",
            'longitud' => -58.288459,
            'latitud' => -26.879844,
            'direccion' => "Eusebio Ayala casi Intendentes Militares 123",
            'hora_apertura' => 8,
            'hora_cierre' => 21,
        ]);
        DB::table('local_donacion')->insert([
            'local_donacion' => "EL CANTARO",
            'longitud' => -56.820182,
            'latitud' => -27.387026,
            'direccion' => "Av. Paraguay Casi Mcal. Lopez 343",
            'hora_apertura' => 8,
            'hora_cierre' => 21,
        ]);
        DB::table('local_donacion')->insert([
            'local_donacion' => "Surubi-i, Mariano",
            'longitud' => -57.521369,
            'latitud' => -25.194156,
            'direccion' => "Ruta Transchaco 1232 casi las Lomas Km. 3",
            'hora_apertura' => null,
            'hora_cierre' => null,
        ]);
        DB::table('local_donacion')->insert([
            'local_donacion' => "SODEP",
            'longitud' => -57.583222,
            'latitud' => -25.308953,
            'direccion' => "Av. Paraguay Casi Mcal. Lopez 343",
            'hora_apertura' => 8,
            'hora_cierre' => 21,
        ]);
        DB::table('local_donacion')->insert([
            'local_donacion' => "ROSHKA",
            'longitud' => -57.6100684,
            'latitud' => -25.2818691,
            'direccion' => null,
            'hora_apertura' => null,
            'hora_cierre' => null,
        ]);
    }
}
