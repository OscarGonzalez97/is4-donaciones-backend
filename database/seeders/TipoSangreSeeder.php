<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoSangreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "A+",
        ]);
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "A-",
        ]);
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "B+",
        ]);
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "B-",
        ]);
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "O+",
        ]);
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "O-",
        ]);
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "AB+",
        ]);
        DB::table('tipo_sangre')->insert([
            'tipo_sangre' => "AB-",
        ]);
    }
}
