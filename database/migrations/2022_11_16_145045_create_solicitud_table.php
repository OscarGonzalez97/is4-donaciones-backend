<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('solicitud');
            $table->unsignedBigInteger('creado_por')->nullable();
            $table->foreign('creado_por')->references('id')->on('users');
            $table->timestamp('fecha_limite', 0)->nullable();
            $table->unsignedBigInteger('id_estado')->nullable()->default(1);
            $table->foreign('id_estado')->references('id')->on('estado');
            $table->integer('volumenes_necesarios');
            $table->string('nombre_apellido_donatario');
            $table->string('cedula_donatario');
            $table->string('telefono_contacto');
            $table->unsignedBigInteger('tipo_sangre');
            $table->foreign('tipo_sangre')->references('id')->on('tipo_sangre');
            $table->string('establecimiento'); //lugar donde va donar
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud');
    }
};
