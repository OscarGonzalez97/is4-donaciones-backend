<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('local_donacion', function (Blueprint $table) {
            $table->string('direccion')->nullable();
            $table->integer('hora_apertura')->nullable();
            $table->integer('hora_cierre')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('local_donacion', function (Blueprint $table) {
            $table->dropColumn('direccion');
        });
        Schema::table('local_donacion', function (Blueprint $table) {
            $table->dropColumn('hora_apertura');
        });
        Schema::table('local_donacion', function (Blueprint $table) {
            $table->dropColumn('hora_cierre');
        });
    }
};
