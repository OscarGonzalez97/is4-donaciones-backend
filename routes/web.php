<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('forgot_password', 'auth.reset_password')->name('password.reset');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/solicitudes', [App\Http\Controllers\HomeController::class, 'solicitudes'])->name('solicitudes');
Route::get('/locales', [App\Http\Controllers\HomeController::class, 'locales'])->name('locales');
Route::get('/users', [App\Http\Controllers\HomeController::class, 'users'])->name('users');
Route::get('/info', [App\Http\Controllers\HomeController::class, 'info'])->name('info');
