<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\LocalDonacion;
use App\Models\Solicitud;
use App\Models\TipoSangre;
use App\Http\Resources\LocalDonacionCollection;
use App\Http\Resources\SolicitudCollection;
use App\Http\Resources\TipoSangreCollection;
use App\Http\Controllers\SolicitudController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CertificadoController;
use App\Http\Controllers\ForgotPasswordController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Usuarios
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/users', function (Request $request) {
    return \App\Models\User::all();
});
Route::post('/reset-password', [ForgotPasswordController::class, 'forgot']);
Route::post('password/reset', [ForgotPasswordController::class, 'reset']);
Route::middleware('auth:sanctum')->post('/editar-perfil', [AuthController::class, 'editar']);
Route::middleware('auth:sanctum')->post('/cambiar-password', [AuthController::class, 'cambiar_password']);
// Auth
Route::post('/registro', [AuthController::class, 'registro']);
Route::post('/login', [AuthController::class, 'login']);

// Locales
Route::get('/locales', function () {
    return new LocalDonacionCollection(LocalDonacion::all());
});
Route::get('/tipos-sangre', function () {
    return new TipoSangreCollection(TipoSangre::all());
});

// Solicitudes
Route::get('/solicitudes', [SolicitudController::class, 'index']);
Route::middleware('auth:sanctum')->get('/solicitudes-protegido', [SolicitudController::class, 'indexProtegido']);
Route::middleware('auth:sanctum')->post('/solicitudes', [SolicitudController::class, 'store']);
Route::middleware('auth:sanctum')->delete('/solicitudes/{id}', [SolicitudController::class, 'destroy']);

// Certificados
Route::middleware('auth:sanctum')->post('/certificados', [CertificadoController::class, 'store']);
Route::middleware('auth:sanctum')->get('/certificados', [CertificadoController::class, 'index']);
//Route::middleware('auth:sanctum')->delete('/certificados/{id}', [CertificadoController::class, 'destroy']);
