<?php

namespace App\Http\Controllers;

use App\Models\LocalDonacion;
use App\Models\Solicitud;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $locales = Solicitud::all()->sortBy('fecha_limite')->values()->all();
            return response()->json(
                [
                    'data' => $locales,
                ],
                200
            );
        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function indexProtegido(Request $request)
    {
        try {
            $locales = Solicitud::where('creado_por', '=', $request->user()->id)->get();
            return response()->json(
                [
                    'data' => $locales,
                ],
                200
            );
        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $user = $request->user();
            $payload = json_decode($request->getContent(), true);
            $solicitud = new Solicitud;
            $solicitud->solicitud = $payload['solicitud'];
            $solicitud->fecha_limite = $payload['fecha_limite'];
            $solicitud->volumenes_necesarios = intval($payload['volumenes_necesarios']);
            $solicitud->nombre_apellido_donatario = $payload['nombre_apellido_donatario'];
            $solicitud->cedula_donatario = $payload['cedula_donatario'];
            $solicitud->telefono_contacto = $payload['telefono_contacto'];
            $solicitud->tipo_sangre = $payload['tipo_sangre'];
            $solicitud->establecimiento = $payload['establecimiento'];
            $solicitud->creado_por = $user->id;
            $solicitud->save();
            // Return success
            return response()->json(
                [
                    'status' => '200',
                    'data' => $solicitud,
                    'message' => 'created'
                ],
                200
            );
        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $user = $request->user();
        $solicitud = Solicitud::findOrFail($id);

        if ($solicitud && $solicitud->creado_por == $user->id) {
            $solicitud->delete();
        } else if ($solicitud->creado_por != $user->id) {
            return response()->json("No es tu solicitud", 403);
        } else {
            return response()->json("No se pudo borrar", 400);
        }
        return response()->json(null);
    }
}
