<?php

namespace App\Http\Controllers;

use App\Models\Certificado;
use App\Models\LocalDonacion;
use App\Models\User;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use function Sodium\add;

class CertificadoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $user = $request->user();
        try {
            $payload = json_decode($request->getContent(), true);

            $puede_donar = true;

            $ultima_donacion = $user->certificados()->latest('fecha_donacion')->first();
            if ($ultima_donacion) {
                $fecha_donacion = new DateTime($payload['fecha_donacion']);
                $ultima_donacion = new DateTime($ultima_donacion->fecha_donacion);
                $diferencia_dias = $fecha_donacion->diff($ultima_donacion)->days;

                // checkeamos que no haya creado otro certificado en el rango de dias
                if ($user->sexo == 'H') {
                    // puede donar cada 3 meses
                    if ($diferencia_dias < 90) {
                        $puede_donar = false;
                    }
                } else {
                    // puede donar cada 4 meses si es mujer o si no establecio el sexo
                    if ($diferencia_dias < 120) {
                        $puede_donar = false;
                    }
                }
            }

            if (!$puede_donar) {
                return response()->json([
                    'status' => false,
                    'message' => 'No puede donar aún'
                ], 400);
            }

            $certificado = new Certificado;
            $certificado->fecha_donacion = $payload['fecha_donacion'];
            $certificado->user_id = $user->id;
            $certificado->local_donacion_id = intval($payload['local_donacion_id']);
            $certificado->save();
            $ultima_donacion = $user->certificados()->latest('fecha_donacion')->first();
            $user->ult_vez_donado = $ultima_donacion->fecha_donacion;
            $user->save();
            // Return success
            return response()->json(
                [
                    'status' => true,
                    'data' => $user,
                    'message' => 'created'
                ],
                200
            );
        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $certificados = $request->user()->certificados;
            if($request['asc']) {
                $certificados = $certificados->sortBy('fecha_donacion')->values()->all();
            } else if ($request['desc']) {
                $certificados = $certificados->sortByDesc('fecha_donacion')->values()->all();
            }

            $data = [];

            // certificados segun lo que necesitamos
            foreach ($certificados as &$certificado) {
                $user = User::where('id', $certificado->user_id)->first();
                array_push($data, [
                    'id' => $certificado->id,
                    'fecha_donacion' => $certificado->fecha_donacion,
                    'user' => $user,
                    'local_donacion' => LocalDonacion::where('id', $certificado->local_donacion_id)->first()->local_donacion,
                ]);
            }

            return response()->json(
                [
                    'status' => '200',
                    'data' => $data,
                    'message' => 'created'
                ],
                200
            );
        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}
