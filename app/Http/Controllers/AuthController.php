<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Throwable;

class AuthController extends Controller
{
    /**
     * Create User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registro(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
                [
                    'name' => 'required',
                    'surname' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'nro_cedula' => 'required',
                    'password' => 'required'
                ]);

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if (User::where('nro_cedula', '=', $request->nro_cedula)->exists()) {
                return response()->json([
                    'status' => false,
                    'message' => 'Cedula existente',
                ], 401);
            }

            $sexo = null;
            if (($request->sexo == 'H' || $request->sexo == 'M')) {
                $sexo = $request->sexo;
            }

            $user = User::create([
                'name' => $request->name,
                'surname' => $request->surname,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'sexo' => $sexo,
                'nro_cedula' => $request->nro_cedula,
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Se creo el usuario correctamente',
                'token' => $user->createToken("API TOKEN")->plainTextToken,
                'user' => $user
            ], 200);

        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * Login The User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required'
                ]);

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'error de validación',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if (!Auth::attempt($request->only(['email', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'Email y/o contraseña incorrectos',
                ], 401);
            }

            $user = User::where('email', $request->email)->first();

            return response()->json([
                'status' => true,
                'message' => 'Ingresó correctamente',
                'token' => $user->createToken("API TOKEN")->plainTextToken,
                'user' => $user
            ], 200);

        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    /**
     * editar The User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editar(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
                [
                    'name' => 'required',
                    'surname' => 'required',
                    'email' => 'required',
                ]);

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            $user = $request->user();

            if ($request->email != $user->email && User::where('email', '=', $request->email)->exists()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => ['email' => 'Email ya existe']
                ], 401);
            }

            $user->name = $request->name;
            $user->surname = $request->surname;
            $user->fecha_nacimiento = $request->fecha_nacimiento;
            $user->email = $request->email;
            $user->sexo = $request->sexo;
            $user->save();

            return response()->json([
                'status' => true,
                'message' => 'Se actualizó correctamente',
                'user' => $user
            ], 200);

        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }


    /**
     * cambiar password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cambiar_password(Request $request)
    {
        try {
            //Validated
            $validateUser = Validator::make($request->all(),
                [
                    'old_password' => 'required',
                    'password' => 'required',
                ]);

            if ($validateUser->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }
            $user = $request->user();

            if (!Hash::check($request['old_password'], $user->password)) {
                return response()->json([
                    'status' => false,
                    'message' => 'Contraseña no válida'
                ], 401);
            }

            $user->password = Hash::make($request->password);
            $user->save();

            return response()->json([
                'status' => true,
                'message' => 'Se actualizó correctamente',
            ], 200);

        } catch (Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}
