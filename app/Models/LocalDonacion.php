<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalDonacion extends Model
{
    use HasFactory;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'local_donacion';

    protected $fillable = ['local_donacion', 'longitud', 'latitud', 'direccion', 'hora_apertura', 'hora_cierre'];

    public $timestamps = false;

}
