<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    use HasFactory;
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'solicitud';

    protected $fillable = [
        'solicitud',
        'fecha_limite',
        'id_estado',
        'volumenes_necesarios',
        'nombre_apellido_donatario',
        'cedula_donatario',
        'telefono_contacto',
        'tipo_sangre',
        'establecimiento',
        'creado_por',
    ];
}
