@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <article class="hentry post" id="content_interna">

                                <div class="entry-meta">
                                    <span class="entry-category"><a href="noticias.php">Noticias</a></span> /
                                    <span class="entry-category"> Banco de sangre del IPS recibe en promedio 25 mil donantes voluntarios al año </span>
                                </div>

                                <h1 class="page-title"> Banco de sangre del IPS recibe en promedio 25 mil donantes voluntarios al año </h1>

                                <div class="entry-content">


                                    <div id="content-descripcion">
                                        <p style="text-align:justify">Al año se tiene un promedio de 25000 donantes voluntarios en el Instituto de Previsión Social.&nbsp;</p>

                                        <p style="text-align:justify">“Mediante nuestros donantes nosotros podemos tratar a nuestros pacientes, gracias a su solidaridad, donar es dar vida”, expresó la Dra. Romy Alcaraz, jefa de la Unidad de Medicina Transfusional.</p>

                                        <p style="text-align:justify">A su vez señaló “felicitamos a las personas que vienen a dar un poco de sí para salvar otras vidas, y a los que aún no donan les instamos a que se animen y que nunca es tarde para hacerlo, no duele, no debilita ni engorda”, mencionó Alcaraz.</p>

                                        <p style="text-align:justify">La sangre es un recurso importante en todos los tratamientos programados y en las intervenciones urgentes. Permite aumentar la esperanza y la calidad de vida de los pacientes con enfermedades potencialmente mortales y llevar a cabo procedimientos médicos y quirúrgicos complejos.</p>

                                        <p style="text-align:justify">Asimismo, es fundamental para tratar a los heridos durante las emergencias de todo tipo y cumple una función esencial en la atención materna y perinatal.</p>

                                        <h4 style="text-align:justify"><span style="color:#16a085">Requisitos para donar</span></h4>

                                        <ul>
                                            <li style="text-align: justify;">Para donar es requisito gozar de buena salud, no haber tenido hepatitis después de los 10 años de edad, tener entre 18 y 65 años, pesar más de 55 kilos, llevar un estilo de vida saludable, no tener comportamiento de riesgo para enfermedades de transmisión sexual, no usar drogas, no estar embarazada o en periodo de lactancia. Luego de un parto se debe aguardar 3 meses, y posterior a una cesárea, 6 meses, No haberse hecho tatuaje en los últimos 12 meses son algunas de los requisitos para la donación.</li>
                                            <li style="text-align: justify;">La menstruación y la ingesta de anticonceptivos orales no impiden la donación, y se puede donar hasta 3 veces por año, en el caso de las mujeres, y 4 veces los hombres.</li>
                                            <li style="text-align: justify;">El Banco de Sangre del Hospital Central del Instituto de Previsión Social está abierto de lunes a viernes de 6:00 a 18:00 horas y los sábados, domingos y feriados de 7:00 a 12:00 horas donde llegan a diario personas dispuestas a donar para salvar y/ o mejorar la calidad de vida de familiares, amigos y hasta extraños que la precisen.</li>
                                            <li style="text-align: justify;">&nbsp;Al donar se extrae 430 CC de sangre, si se tiene 55 o más kilos se pueden donar sin ningún inconveniente. Serán realizados exámenes para Hepatitis B y C, Sífilis, Chagas, HIV y para detección de HTLVI I y II conforme a las normativas vigentes.</li>
                                        </ul>
                                    </div>


                                    <!-- SECCION PARA COMPARTIR EN REDES SOCIALES -->



                                </div><!-- .entry-content -->

                            </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
