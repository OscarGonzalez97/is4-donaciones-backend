<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LocalDonacionTest extends TestCase
{
    public function testGettingAllLocales()
    {
        $response = $this->get('api/locales');
        $response->assertStatus(200);

        $response->assertJsonStructure(
            [ 'data' =>
                array(
                    [
                    'id',
                    'local_donacion',
                    'longitud',
                    'latitud',
                    'creado_por'
                    ]
                )
            ]
        );
    }
}
